@extends ('layout/main')

@section('title','Daftar barang')

@section('container')
    <div class="container">
      <div class="row">
        <div class="col-10">
          <h1 class="mt-3">Daftar barang di keranjang</h1>
          <table class="table">
  			<thead class="table-dark">
    			<tr>
	    			<th scope="col">#</th>
	    			<th scope="col">Nama Barang</th>
	    			<th scope="col">ID</th>
	    			<th scope="col">Jumlah barang</th>
	    			<th scope="col">Aksi</th>
    			</tr>
  			</thead>
  			<tbody>
  				@foreach($cart as $list)
  				<tr>
  					<th scope='row'>{{$loop -> iteration}}</th>
  					<td>{{$list ->nama}}</td>
  					<td>{{$list ->id}}</td>
  					<td>{{$list ->jumlah}}</td>
  					<td>
  						<button type="button" class="btn btn-success">Edit</button>
  						<button type="button" class="btn btn-danger">Delete</button>
  					</td>
  				</tr>
  				@endforeach
  			</tbody>
		  </table>

        </div>
      </div>
    </div>
@endsection