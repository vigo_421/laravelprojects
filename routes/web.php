<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

// Route::get('/contactus', function () {
//     return view('contactus');
// });

// Route::get('/diningroom', function () {
//     return view('diningroom');
// });

// Route::get('/livingroom', function () {
//     return view('livingroom');
// });

// Route::get('/kitchen', function () {
//     return view('kitchen');
// });

Route::get('/', 'App\Http\Controllers\PagesController@home');
Route::get('/contactus', 'App\Http\Controllers\PagesController@contactus');
Route::get('/livingroom', 'App\Http\Controllers\PagesController@livingroom');
Route::get('/diningroom', 'App\Http\Controllers\PagesController@diningroom');
Route::get('/kitchen', 'App\Http\Controllers\PagesController@kitchen');

Route::get('/cart', 'App\Http\Controllers\CartController@index');
