<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Http\Controllers\Controller;

class PagesController extends Controller
{
	public function home(){
		return view('home');
	}
	public function contactus(){
		return view('contactus');
	}
	public function livingroom(){
		return view('livingroom');
	}
	public function diningroom(){
		return view('diningroom');
	}
	public function kitchen(){
		return view('kitchen');
	}
}
